@extends('layouts.main')

<div class="container">
    <div class="card">
        <div class="card-header">
            <h1>Editar Série</h1>
        </div>
        <div class="card-body">
            <div class="d-flex flex-row bd-highlight mb-3">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="container col-4">
                    <form action="{{route('series.update',$serie->id)}}" method="post">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="id" value="{{$serie->id}}">
                        <label class="col-form-label" for="">Nome</label>
                        <div class="input-group">
                            <input name="nome" class="form-control col" type="text" value="{{$serie->nome}}">
                        </div>
                        <div>
                            <button class="btn btn-primary mt-2 mb-1" type="submit">Salvar</button>
                        </div>
                    </form>
                </div>
                <div class="container col-4"></div>
                <div class="container col-4"></div>
            </div>
        </div>
    </div>
</div>
